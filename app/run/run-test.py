from runner import Runner


runner = Runner()
data = {"code_test_table_id": 21443, "lang": "python",
        "source": "print(input())", "argument": "123", "solution": "123\\n"}
runner.set_data(data)
runner.set_lang()
runner.make_file()
runner.container_image_build()
runner.run_code()
print(runner.check_results())
