import os
import docker
import json
import requests
import sys


try:
    Nagi_Api = os.environ["Nagi_Api"]
except BaseException as e:
    pass


class Runner(object):
    def __init__(self):
        self.lang = None
        self.code_test_table_id = None
        self.source = None
        self.source_filename = None
        self.argument = None
        self.extension = None
        self.cmd = None
        self.bind_source_file = None
        self.path = None
        self.result = None
        self.solution = None
        self.judge = False
        self.container_image_id = None
        self.bind_source_file_name = None

    def set_data(self, json_data):
        code_test_table_id_key = "code_test_table_id" in json_data
        lang_key = "lang" in json_data
        source_key = "source" in json_data
        argument_key = "argument" in json_data
        solution = "solution" in json_data  # 想定解
        if code_test_table_id_key and lang_key and source_key and argument_key:
            self.code_test_table_id = json_data["code_test_table_id"]
            self.lang = json_data["lang"]
            self.source = json_data["source"]
            self.argument = json_data["argument"]
            self.solution = json_data["solution"]

    def get_extension(self):
        return self.extension

    def get_cmd(self):
        return self.cmd

    def get_bind_source_file(self):
        # コンテナ内のどこにバインドするかのパス
        # コンテン内で使えるディレクトリパスが入っている
        '''
        # コンテナ
        '''
        return self.bind_source_file

    def get_code_test_table_id(self):
        return self.code_test_table_id

    def get_source(self):
        return self.source

    def get_path(self):
        # ホスト場所
        '''
        # ホスト
        '''
        return self.path

    def get_solution(self):
        # 想定解
        return self.solution

    def get_result(self):
        return self.result

    def get_source_filename(self):
        return self.source_filename

    def set_container_image_id(self, container_image_id):
        self.container_image_id = container_image_id.split(":")[1]

    def get_container_image_id(self):
        return self.container_image_id

    def make_file(self):
        code_test_table_id = self.get_code_test_table_id()
        extension = self.get_extension()
        source = self.get_source()
        self.source_filename = 'code-' + str(code_test_table_id) + extension
        self.path = os.path.dirname(os.path.abspath(
            __file__)) + '/code/' + self.source_filename

        print("path: {0}".format(self.path))
        with open(self.path, mode='w') as f:
            f.write(source)

    def set_python(self):
        self.extension = ".py"
        self.cmd = 'bash /starter/code-start.sh python3 /source/main.py'
        split_arg = self.argument.splitlines()
        for i in split_arg:
            self.cmd += " "+i
        self.bind_source_file = '/source/main.py'
        self.bind_source_file_name = "main.py"
        print("command: {0}".format(self.cmd))

    def set_clang(self):
        self.extension = ".c"
        self.cmd = "bash /starter/code-start.sh Clang /source/main.c"
        split_arg = self.argument.splitlines()
        for i in split_arg:
            self.cmd += " " + i
        self.bind_source_file = '/source/main.c'
        self.bind_source_file_name = "main.c"
        print("command: {0}".format(self.cmd))

    def set_lang(self):
        if self.lang == "python":
            print("set lang: {0}".format("python"))
            self.set_python()
        elif self.lang == "Clang":
            print("set lang: {0}".format("Clang"))
            self.set_clang()
        else:
            raise "don't seach lang"

    def run_code(self):
        cmd = self.get_cmd()
        code_test_table_id = self.get_code_test_table_id()
        path = self.get_path()
        bind_source_file = self.get_bind_source_file()
        image_id = self.get_container_image_id()
        print("container image id: {0}".format(image_id))
        try:
            client = docker.DockerClient(base_url='unix://var/run/docker.sock')
            print("ping: {0}".format(client.ping()))
            result = client.containers.run(
                image=image_id,
                command=cmd,
                # volumes={path: {'bind': bind_source_file, 'mode': 'rw'}},
                remove=True,
                tty=True
            )
            print("result: {0}".format(result))
            self.result = result.decode('utf-8')
        except BaseException as e:
            print("Err: {0}".format(e))
            self.result = "Code: Err"
        return self.result

    def send_result_api_server(self):
        code_test_table_id = self.code_test_table_id
        result = self.result
        judge = self.judge
        url = "http://{0}:8080/code/results".format(Nagi_Api)
        data = {
            "code_test_table_id": code_test_table_id,
            "result": result,
            "judge": judge,
        }
        data = json.dumps(data)
        header = {'content-type': 'application/json'}
        requests.post(url, data=data, headers=header)

    def check_results(self):
        # 回答アルゴリズム
        result = self.get_result()
        solution = self.get_solution()
        if result is None or solution is None:
            return self.judge
        else:
            result = result.replace('\r', "").replace('\n', '\\n')
            self.result = result
            print(result)
            print(solution == result)
            self.judge = solution == result
        return self.judge  # True or False

    def container_image_build(self):
        client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        source_file = "code/" + self.get_source_filename()
        file_name = self.bind_source_file_name
        print("file_name: {0}".format(file_name))
        try:
            code_file_path = self.get_path()
            buildargs = {"HOSTCODEFILE": source_file,
                         "CONTAINERCODEFILE": file_name}

            # print(buildargs)
            image = client.images.build(
                path="./app/run/", buildargs=buildargs, tag="code-test-image")
            self.set_container_image_id(image[0].id)
            print(self.get_container_image_id())
        except BaseException as e:
            print("build: {0}".format(e))

    # def container_image_remove(self):
    #     client = docker.DockerClient(base_url='unix://var/run/docker.sock')
    #     source_file = "code/"+self.get_source_filename()


# runner = Runner()
# data = {"code_test_table_id": 2143, "lang": "python",
#         "source": "print(input())", "argument": "123", "solution": "123\\n"}
# runner.set_data(data)
# runner.set_python()
# runner.make_file()
# runner.container_image_build()
# runner.run_code()
# print(runner.check_results())
