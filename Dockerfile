FROM python:3

WORKDIR /usr/src/app

COPY ./requirements.txt ./
COPY ./app/run ./run
RUN ls -ls
RUN pip install --no-cache-dir -r requirements.txt
RUN mkdir -p /usr/src/app/run/code
WORKDIR /usr/src
CMD [ "python","-u", "app/run/recive.py" ]